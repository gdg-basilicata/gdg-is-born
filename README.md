# GDG is Born

Repository containing material from the event: A GDG is Born -- 05/07/2024 

You can freely consult the slides and code used during the event

# Event Details

GDG Basilicata

GDG Basilicata: un pomeriggio di tecnologia e connessioni. Partecipa all'evento di presentazione del GDG Basilicata!

Sei appassionato di tecnologia e sviluppo? Vuoi conoscere le ultime novità del panorama tech e connetterti con altri appassionati nella tua regione? Allora non perdere l'evento di presentazione del GDG Basilicata che si terrà Venerdì 05 Luglio dalle ore 16,30 alle 18,30 presso Casa BCC Basilicata a Potenza, Basilicata. Un pomeriggio di approfondimenti e connessioni per scoprire cosa fa un GDG e come può supportare la tua crescita professionale, rimanere aggiornato sulle ultime novità di Google I/O (Extended 2024 campaign) e condividere le tue idee e contribuire a definire il futuro del GDG Basilicata. Posti limitati! Non perdere l'occasione di essere parte di una community vibrante e appassionata! Seguici sui social per rimanere aggiornato!!! #GDBG #Basilicata #Tecnologia #Google #Community**
